import os
import sys
from pathlib import Path
from datetime import date

def filerenamer():
	today = date.today()
	dt = today.strftime("%b-%d-%Y")
	path = sys.argv[1]
    for files in os.listdir(path):
        if Path(files).suffix == '.jpg':
            old = os.path.basename(files)
            os.rename(old, dt+old)

filerenamer()